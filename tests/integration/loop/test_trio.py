import asyncio
import sys

import pytest


@pytest.mark.skipif(
    sys.version_info < (3, 7), reason="trio requires at least python 3.7"
)
def test_backend(hub):
    hub.loop.init.create("trio")
    assert "TrioEventLoop" in repr(hub.pop.loop.CURRENT_LOOP)
    assert hub.loop.init.backend() in ("trio", "unknown")
    assert hub.loop.init.BACKEND == "trio"
    hub.pop.Loop.run_until_complete(asyncio.sleep(0))
