import asyncio
import os
import sys

import pytest


@pytest.mark.skipif(os.name == "nt", reason="uvloop does not support Windows")
@pytest.mark.skipif(
    sys.version_info < (3, 7), reason="uvloop requires at least python 3.7"
)
def test_backend(hub):
    hub.loop.init.create("uv")
    assert "uvloop.Loop" in repr(hub.pop.loop.CURRENT_LOOP)
    assert hub.loop.init.backend() == "asyncio"
    assert hub.loop.init.BACKEND == "uv"
    hub.pop.Loop.run_until_complete(asyncio.sleep(0))
